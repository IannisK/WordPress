FROM mysql:8.0

ARG MYSQL_ROOT_PASSWORD
ARG MYSQL_USER
ARG MYSQL_PASSWORD

ENV MYSQL_DATABASE=wordpress
ENV MYSQL_ROOT_PASSWORD=$MYSQL_PASSWORD
ENV MYSQL_USER=$MYSQL_USER
ENV MYSQL_PASSWORD=$MYSQL_PASSWORD

COPY ./db_backup/mysite_db.sql /tmp/mysite_db.sql
COPY ./db_backup/db_import.sh /usr/local/bin
RUN chmod +x /usr/local/bin/db_import.sh

EXPOSE 3306

ENTRYPOINT [ "db_import.sh" ]