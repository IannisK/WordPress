#!/bin/bash

# # Start MySQL in the background
# cd /var/lib/mysql
# mysqld --initialize
# mysqld &

# # Wait for MySQL to be fully up and running
# until mysqladmin ping -h "localhost" --silent; do
#   echo "Waiting for MySQL..."
#   sleep 5
# done

# echo "Importing database..."
# cd /tmp/
# mysql -u root -p${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} < /tmp/mysite_db.sql
# echo "Database imported."
#-------------------------------------------------------------------------------------------------

cd /var/www/html/
export $(grep -v '^#' .env | xargs)
docker exec -i database mysql -h 127.0.0.1 -P 3306 -u ${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} < /db_backup/mysite_db.sql